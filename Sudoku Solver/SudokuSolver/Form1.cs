﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.Windows.Input;


namespace SudokuSolver
{
    public partial class Form1 : Form
    {
        Random rnd = new Random();

        Stopwatch swatch = new Stopwatch();

        int[,] matrixMain = new int[9, 9];
        int[,] matrixFixed = new int[9, 9];

        int[] arrayOfPos = new int[9];
        int countInArrayOfPos;

        bool end = false;
        int grow, gcolumn;
        bool goingBack = false;

        bool success = false;

        int[] used00 = new int[9];
        int[] used01 = new int[9];
        int[] used02 = new int[9];
        int[] used03 = new int[9];
        int[] used04 = new int[9];
        int[] used05 = new int[9];
        int[] used06 = new int[9];
        int[] used07 = new int[9];
        int[] used08 = new int[9];

        int[] used10 = new int[9];
        int[] used11 = new int[9];
        int[] used12 = new int[9];
        int[] used13 = new int[9];
        int[] used14 = new int[9];
        int[] used15 = new int[9];
        int[] used16 = new int[9];
        int[] used17 = new int[9];
        int[] used18 = new int[9];

        int[] used20 = new int[9];
        int[] used21 = new int[9];
        int[] used22 = new int[9];
        int[] used23 = new int[9];
        int[] used24 = new int[9];
        int[] used25 = new int[9];
        int[] used26 = new int[9];
        int[] used27 = new int[9];
        int[] used28 = new int[9];

        int[] used30 = new int[9];
        int[] used31 = new int[9];
        int[] used32 = new int[9];
        int[] used33 = new int[9];
        int[] used34 = new int[9];
        int[] used35 = new int[9];
        int[] used36 = new int[9];
        int[] used37 = new int[9];
        int[] used38 = new int[9];

        int[] used40 = new int[9];
        int[] used41 = new int[9];
        int[] used42 = new int[9];
        int[] used43 = new int[9];
        int[] used44 = new int[9];
        int[] used45 = new int[9];
        int[] used46 = new int[9];
        int[] used47 = new int[9];
        int[] used48 = new int[9];

        int[] used50 = new int[9];
        int[] used51 = new int[9];
        int[] used52 = new int[9];
        int[] used53 = new int[9];
        int[] used54 = new int[9];
        int[] used55 = new int[9];
        int[] used56 = new int[9];
        int[] used57 = new int[9];
        int[] used58 = new int[9];

        int[] used60 = new int[9];
        int[] used61 = new int[9];
        int[] used62 = new int[9];
        int[] used63 = new int[9];
        int[] used64 = new int[9];
        int[] used65 = new int[9];
        int[] used66 = new int[9];
        int[] used67 = new int[9];
        int[] used68 = new int[9];

        int[] used70 = new int[9];
        int[] used71 = new int[9];
        int[] used72 = new int[9];
        int[] used73 = new int[9];
        int[] used74 = new int[9];
        int[] used75 = new int[9];
        int[] used76 = new int[9];
        int[] used77 = new int[9];
        int[] used78 = new int[9];

        int[] used80 = new int[9];
        int[] used81 = new int[9];
        int[] used82 = new int[9];
        int[] used83 = new int[9];
        int[] used84 = new int[9];
        int[] used85 = new int[9];
        int[] used86 = new int[9];
        int[] used87 = new int[9];
        int[] used88 = new int[9];



        public Form1()
        { 
            InitializeComponent();



            InitMatrix();
            eraseArrayOfPos();
        }
       
        private void InitMatrix()
        {
            for (int i = 0; i < 9; i++)
            {             
                for (int j = 0; j < 9; j++)
                {
                    matrixMain[i, j] = 0;
                }
            }
            grow = 0;
            gcolumn = 0;
        }
        private void LoadInMatrix()
        {

            if (tb00.Text != "") matrixMain[0, 0] = Convert.ToInt32(tb00.Text);
            if (tb01.Text != "") matrixMain[0, 1] = Convert.ToInt32(tb01.Text);
            if (tb02.Text != "") matrixMain[0, 2] = Convert.ToInt32(tb02.Text);
            if (tb03.Text != "") matrixMain[0, 3] = Convert.ToInt32(tb03.Text);
            if (tb04.Text != "") matrixMain[0, 4] = Convert.ToInt32(tb04.Text);
            if (tb05.Text != "") matrixMain[0, 5] = Convert.ToInt32(tb05.Text);
            if (tb06.Text != "") matrixMain[0, 6] = Convert.ToInt32(tb06.Text);
            if (tb07.Text != "") matrixMain[0, 7] = Convert.ToInt32(tb07.Text);
            if (tb08.Text != "") matrixMain[0, 8] = Convert.ToInt32(tb08.Text);

            if (tb10.Text != "") matrixMain[1, 0] = Convert.ToInt32(tb10.Text);
            if (tb11.Text != "") matrixMain[1, 1] = Convert.ToInt32(tb11.Text);
            if (tb12.Text != "") matrixMain[1, 2] = Convert.ToInt32(tb12.Text);
            if (tb13.Text != "") matrixMain[1, 3] = Convert.ToInt32(tb13.Text);
            if (tb14.Text != "") matrixMain[1, 4] = Convert.ToInt32(tb14.Text);
            if (tb15.Text != "") matrixMain[1, 5] = Convert.ToInt32(tb15.Text);
            if (tb16.Text != "") matrixMain[1, 6] = Convert.ToInt32(tb16.Text);
            if (tb17.Text != "") matrixMain[1, 7] = Convert.ToInt32(tb17.Text);
            if (tb18.Text != "") matrixMain[1, 8] = Convert.ToInt32(tb18.Text);

            if (tb20.Text != "") matrixMain[2, 0] = Convert.ToInt32(tb20.Text);
            if (tb21.Text != "") matrixMain[2, 1] = Convert.ToInt32(tb21.Text);
            if (tb22.Text != "") matrixMain[2, 2] = Convert.ToInt32(tb22.Text);
            if (tb23.Text != "") matrixMain[2, 3] = Convert.ToInt32(tb23.Text);
            if (tb24.Text != "") matrixMain[2, 4] = Convert.ToInt32(tb24.Text);
            if (tb25.Text != "") matrixMain[2, 5] = Convert.ToInt32(tb25.Text);
            if (tb26.Text != "") matrixMain[2, 6] = Convert.ToInt32(tb26.Text);
            if (tb27.Text != "") matrixMain[2, 7] = Convert.ToInt32(tb27.Text);
            if (tb28.Text != "") matrixMain[2, 8] = Convert.ToInt32(tb28.Text);

            if (tb30.Text != "") matrixMain[3, 0] = Convert.ToInt32(tb30.Text);
            if (tb31.Text != "") matrixMain[3, 1] = Convert.ToInt32(tb31.Text);
            if (tb32.Text != "") matrixMain[3, 2] = Convert.ToInt32(tb32.Text);
            if (tb33.Text != "") matrixMain[3, 3] = Convert.ToInt32(tb33.Text);
            if (tb34.Text != "") matrixMain[3, 4] = Convert.ToInt32(tb34.Text);
            if (tb35.Text != "") matrixMain[3, 5] = Convert.ToInt32(tb35.Text);
            if (tb36.Text != "") matrixMain[3, 6] = Convert.ToInt32(tb36.Text);
            if (tb37.Text != "") matrixMain[3, 7] = Convert.ToInt32(tb37.Text);
            if (tb38.Text != "") matrixMain[3, 8] = Convert.ToInt32(tb38.Text);

            if (tb40.Text != "") matrixMain[4, 0] = Convert.ToInt32(tb40.Text);
            if (tb41.Text != "") matrixMain[4, 1] = Convert.ToInt32(tb41.Text);
            if (tb42.Text != "") matrixMain[4, 2] = Convert.ToInt32(tb42.Text);
            if (tb43.Text != "") matrixMain[4, 3] = Convert.ToInt32(tb43.Text);
            if (tb44.Text != "") matrixMain[4, 4] = Convert.ToInt32(tb44.Text);
            if (tb45.Text != "") matrixMain[4, 5] = Convert.ToInt32(tb45.Text);
            if (tb46.Text != "") matrixMain[4, 6] = Convert.ToInt32(tb46.Text);
            if (tb47.Text != "") matrixMain[4, 7] = Convert.ToInt32(tb47.Text);
            if (tb48.Text != "") matrixMain[4, 8] = Convert.ToInt32(tb48.Text);

            if (tb50.Text != "") matrixMain[5, 0] = Convert.ToInt32(tb50.Text);
            if (tb51.Text != "") matrixMain[5, 1] = Convert.ToInt32(tb51.Text);
            if (tb52.Text != "") matrixMain[5, 2] = Convert.ToInt32(tb52.Text);
            if (tb53.Text != "") matrixMain[5, 3] = Convert.ToInt32(tb53.Text);
            if (tb54.Text != "") matrixMain[5, 4] = Convert.ToInt32(tb54.Text);
            if (tb55.Text != "") matrixMain[5, 5] = Convert.ToInt32(tb55.Text);
            if (tb56.Text != "") matrixMain[5, 6] = Convert.ToInt32(tb56.Text);
            if (tb57.Text != "") matrixMain[5, 7] = Convert.ToInt32(tb57.Text);
            if (tb58.Text != "") matrixMain[5, 8] = Convert.ToInt32(tb58.Text);

            if (tb60.Text != "") matrixMain[6, 0] = Convert.ToInt32(tb60.Text);
            if (tb61.Text != "") matrixMain[6, 1] = Convert.ToInt32(tb61.Text);
            if (tb62.Text != "") matrixMain[6, 2] = Convert.ToInt32(tb62.Text);
            if (tb63.Text != "") matrixMain[6, 3] = Convert.ToInt32(tb63.Text);
            if (tb64.Text != "") matrixMain[6, 4] = Convert.ToInt32(tb64.Text);
            if (tb65.Text != "") matrixMain[6, 5] = Convert.ToInt32(tb65.Text);
            if (tb66.Text != "") matrixMain[6, 6] = Convert.ToInt32(tb66.Text);
            if (tb67.Text != "") matrixMain[6, 7] = Convert.ToInt32(tb67.Text);
            if (tb68.Text != "") matrixMain[6, 8] = Convert.ToInt32(tb68.Text);

            if (tb70.Text != "") matrixMain[7, 0] = Convert.ToInt32(tb70.Text);
            if (tb71.Text != "") matrixMain[7, 1] = Convert.ToInt32(tb71.Text);
            if (tb72.Text != "") matrixMain[7, 2] = Convert.ToInt32(tb72.Text);
            if (tb73.Text != "") matrixMain[7, 3] = Convert.ToInt32(tb73.Text);
            if (tb74.Text != "") matrixMain[7, 4] = Convert.ToInt32(tb74.Text);
            if (tb75.Text != "") matrixMain[7, 5] = Convert.ToInt32(tb75.Text);
            if (tb76.Text != "") matrixMain[7, 6] = Convert.ToInt32(tb76.Text);
            if (tb77.Text != "") matrixMain[7, 7] = Convert.ToInt32(tb77.Text);
            if (tb78.Text != "") matrixMain[7, 8] = Convert.ToInt32(tb78.Text);

            if (tb80.Text != "") matrixMain[8, 0] = Convert.ToInt32(tb80.Text);
            if (tb81.Text != "") matrixMain[8, 1] = Convert.ToInt32(tb81.Text);
            if (tb82.Text != "") matrixMain[8, 2] = Convert.ToInt32(tb82.Text);
            if (tb83.Text != "") matrixMain[8, 3] = Convert.ToInt32(tb83.Text);
            if (tb84.Text != "") matrixMain[8, 4] = Convert.ToInt32(tb84.Text);
            if (tb85.Text != "") matrixMain[8, 5] = Convert.ToInt32(tb85.Text);
            if (tb86.Text != "") matrixMain[8, 6] = Convert.ToInt32(tb86.Text);
            if (tb87.Text != "") matrixMain[8, 7] = Convert.ToInt32(tb87.Text);
            if (tb88.Text != "") matrixMain[8, 8] = Convert.ToInt32(tb88.Text);


            //popunjava stalnu matricu
            for(int i = 0; i < 9; i++)
            {
                for (int j = 0;j < 9; j++)
                {
                    matrixFixed[i, j] = matrixMain[i, j];
                }
            }


        }
        private void eraseArrayOfPos()
        {
            for(int x = 0; x < 9; x++)
            {
                arrayOfPos[x] = 0;
            }
            countInArrayOfPos = 0;
        }
     
        private bool isUsed(int row, int column, int num)
        {
            if (row == 0 && column == 0) return checkUsedIn00(num);
            else if (row == 0 && column == 1) return checkUsedIn01(num);
            else if (row == 0 && column == 2) return checkUsedIn02(num);
            else if (row == 0 && column == 3) return checkUsedIn03(num);
            else if (row == 0 && column == 4) return checkUsedIn04(num);
            else if (row == 0 && column == 5) return checkUsedIn05(num);
            else if (row == 0 && column == 6) return checkUsedIn06(num);
            else if (row == 0 && column == 7) return checkUsedIn07(num);
            else if (row == 0 && column == 8) return checkUsedIn08(num);

            else if (row == 1 && column == 0) return checkUsedIn10(num);
            else if (row == 1 && column == 1) return checkUsedIn11(num);
            else if (row == 1 && column == 2) return checkUsedIn12(num);
            else if (row == 1 && column == 3) return checkUsedIn13(num);
            else if (row == 1 && column == 4) return checkUsedIn14(num);
            else if (row == 1 && column == 5) return checkUsedIn15(num);
            else if (row == 1 && column == 6) return checkUsedIn16(num);
            else if (row == 1 && column == 7) return checkUsedIn17(num);
            else if (row == 1 && column == 8) return checkUsedIn18(num);

            else if (row == 2 && column == 0) return checkUsedIn20(num);
            else if (row == 2 && column == 1) return checkUsedIn21(num);
            else if (row == 2 && column == 2) return checkUsedIn22(num);
            else if (row == 2 && column == 3) return checkUsedIn23(num);
            else if (row == 2 && column == 4) return checkUsedIn24(num);
            else if (row == 2 && column == 5) return checkUsedIn25(num);
            else if (row == 2 && column == 6) return checkUsedIn26(num);
            else if (row == 2 && column == 7) return checkUsedIn27(num);
            else if (row == 2 && column == 8) return checkUsedIn28(num);

            else if (row == 3 && column == 0) return checkUsedIn30(num);
            else if (row == 3 && column == 1) return checkUsedIn31(num);
            else if (row == 3 && column == 2) return checkUsedIn32(num);
            else if (row == 3 && column == 3) return checkUsedIn33(num);
            else if (row == 3 && column == 4) return checkUsedIn34(num);
            else if (row == 3 && column == 5) return checkUsedIn35(num);
            else if (row == 3 && column == 6) return checkUsedIn36(num);
            else if (row == 3 && column == 7) return checkUsedIn37(num);
            else if (row == 3 && column == 8) return checkUsedIn38(num);

            else if (row == 4 && column == 0) return checkUsedIn40(num);
            else if (row == 4 && column == 1) return checkUsedIn41(num);
            else if (row == 4 && column == 2) return checkUsedIn42(num);
            else if (row == 4 && column == 3) return checkUsedIn43(num);
            else if (row == 4 && column == 4) return checkUsedIn44(num);
            else if (row == 4 && column == 5) return checkUsedIn45(num);
            else if (row == 4 && column == 6) return checkUsedIn46(num);
            else if (row == 4 && column == 7) return checkUsedIn47(num);
            else if (row == 4 && column == 8) return checkUsedIn48(num);

            else if (row == 5 && column == 0) return checkUsedIn50(num);
            else if (row == 5 && column == 1) return checkUsedIn51(num);
            else if (row == 5 && column == 2) return checkUsedIn52(num);
            else if (row == 5 && column == 3) return checkUsedIn53(num);
            else if (row == 5 && column == 4) return checkUsedIn54(num);
            else if (row == 5 && column == 5) return checkUsedIn55(num);
            else if (row == 5 && column == 6) return checkUsedIn56(num);
            else if (row == 5 && column == 7) return checkUsedIn57(num);
            else if (row == 5 && column == 8) return checkUsedIn58(num);

            else if (row == 6 && column == 0) return checkUsedIn60(num);
            else if (row == 6 && column == 1) return checkUsedIn61(num);
            else if (row == 6 && column == 2) return checkUsedIn62(num);
            else if (row == 6 && column == 3) return checkUsedIn63(num);
            else if (row == 6 && column == 4) return checkUsedIn64(num);
            else if (row == 6 && column == 5) return checkUsedIn65(num);
            else if (row == 6 && column == 6) return checkUsedIn66(num);
            else if (row == 6 && column == 7) return checkUsedIn67(num);
            else if (row == 6 && column == 8) return checkUsedIn68(num);

            else if (row == 7 && column == 0) return checkUsedIn70(num);
            else if (row == 7 && column == 1) return checkUsedIn71(num);
            else if (row == 7 && column == 2) return checkUsedIn72(num);
            else if (row == 7 && column == 3) return checkUsedIn73(num);
            else if (row == 7 && column == 4) return checkUsedIn74(num);
            else if (row == 7 && column == 5) return checkUsedIn75(num);
            else if (row == 7 && column == 6) return checkUsedIn76(num);
            else if (row == 7 && column == 7) return checkUsedIn77(num);
            else if (row == 7 && column == 8) return checkUsedIn78(num);

            else if (row == 8 && column == 0) return checkUsedIn80(num);
            else if (row == 8 && column == 1) return checkUsedIn81(num);
            else if (row == 8 && column == 2) return checkUsedIn82(num);
            else if (row == 8 && column == 3) return checkUsedIn83(num);
            else if (row == 8 && column == 4) return checkUsedIn84(num);
            else if (row == 8 && column == 5) return checkUsedIn85(num);
            else if (row == 8 && column == 6) return checkUsedIn86(num);
            else if (row == 8 && column == 7) return checkUsedIn87(num);
            else return checkUsedIn88(num);


        }
       
        private void insertIntoUsed(int row, int column, int num)
        {
            if (row == 0 && column == 0)  insertIn00(num);
            else if (row == 0 && column == 1) insertIn01(num);
            else if (row == 0 && column == 2) insertIn02(num);
            else if (row == 0 && column == 3) insertIn03(num);
            else if (row == 0 && column == 4) insertIn04(num);
            else if (row == 0 && column == 5) insertIn05(num);
            else if (row == 0 && column == 6) insertIn06(num);
            else if (row == 0 && column == 7) insertIn07(num);
            else if (row == 0 && column == 8) insertIn08(num);

            else if (row == 1 && column == 0) insertIn10(num);
            else if (row == 1 && column == 1) insertIn11(num);
            else if (row == 1 && column == 2) insertIn12(num);
            else if (row == 1 && column == 3) insertIn13(num);
            else if (row == 1 && column == 4) insertIn14(num);
            else if (row == 1 && column == 5) insertIn15(num);
            else if (row == 1 && column == 6) insertIn16(num);
            else if (row == 1 && column == 7) insertIn17(num);
            else if (row == 1 && column == 8) insertIn18(num);

            else if (row == 2 && column == 0) insertIn20(num);
            else if (row == 2 && column == 1) insertIn21(num);
            else if (row == 2 && column == 2) insertIn22(num);
            else if (row == 2 && column == 3) insertIn23(num);
            else if (row == 2 && column == 4) insertIn24(num);
            else if (row == 2 && column == 5) insertIn25(num);
            else if (row == 2 && column == 6) insertIn26(num);
            else if (row == 2 && column == 7) insertIn27(num);
            else if (row == 2 && column == 8) insertIn28(num);

            else if (row == 3 && column == 0) insertIn30(num);
            else if (row == 3 && column == 1) insertIn31(num);
            else if (row == 3 && column == 2) insertIn32(num);
            else if (row == 3 && column == 3) insertIn33(num);
            else if (row == 3 && column == 4) insertIn34(num);
            else if (row == 3 && column == 5) insertIn35(num);
            else if (row == 3 && column == 6) insertIn36(num);
            else if (row == 3 && column == 7) insertIn37(num);
            else if (row == 3 && column == 8) insertIn38(num);

            else if (row == 4 && column == 0) insertIn40(num);
            else if (row == 4 && column == 1) insertIn41(num);
            else if (row == 4 && column == 2) insertIn42(num);
            else if (row == 4 && column == 3) insertIn43(num);
            else if (row == 4 && column == 4) insertIn44(num);
            else if (row == 4 && column == 5) insertIn45(num);
            else if (row == 4 && column == 6) insertIn46(num);
            else if (row == 4 && column == 7) insertIn47(num);
            else if (row == 4 && column == 8) insertIn48(num);

            else if (row == 5 && column == 0) insertIn50(num);
            else if (row == 5 && column == 1) insertIn51(num);
            else if (row == 5 && column == 2) insertIn52(num);
            else if (row == 5 && column == 3) insertIn53(num);
            else if (row == 5 && column == 4) insertIn54(num);
            else if (row == 5 && column == 5) insertIn55(num);
            else if (row == 5 && column == 6) insertIn56(num);
            else if (row == 5 && column == 7) insertIn57(num);
            else if (row == 5 && column == 8) insertIn58(num);

            else if (row == 6 && column == 0) insertIn60(num);
            else if (row == 6 && column == 1) insertIn61(num);
            else if (row == 6 && column == 2) insertIn62(num);
            else if (row == 6 && column == 3) insertIn63(num);
            else if (row == 6 && column == 4) insertIn64(num);
            else if (row == 6 && column == 5) insertIn65(num);
            else if (row == 6 && column == 6) insertIn66(num);
            else if (row == 6 && column == 7) insertIn67(num);
            else if (row == 6 && column == 8) insertIn68(num);

            else if (row == 7 && column == 0) insertIn70(num);
            else if (row == 7 && column == 1) insertIn71(num);
            else if (row == 7 && column == 2) insertIn72(num);
            else if (row == 7 && column == 3) insertIn73(num);
            else if (row == 7 && column == 4) insertIn74(num);
            else if (row == 7 && column == 5) insertIn75(num);
            else if (row == 7 && column == 6) insertIn76(num);
            else if (row == 7 && column == 7) insertIn77(num);
            else if (row == 7 && column == 8) insertIn78(num);

            else if (row == 8 && column == 0) insertIn80(num);
            else if (row == 8 && column == 1) insertIn81(num);
            else if (row == 8 && column == 2) insertIn82(num);
            else if (row == 8 && column == 3) insertIn83(num);
            else if (row == 8 && column == 4) insertIn84(num);
            else if (row == 8 && column == 5) insertIn85(num);
            else if (row == 8 && column == 6) insertIn86(num);
            else if (row == 8 && column == 7) insertIn87(num);
            else insertIn88(num);

        }

        private void insertIn00(int num)
        {
            int i = 0;
            bool found = false;
            while(found == false)
            {
                if (used00[i] == 0)
                {
                    used00[i] = num;
                    found = true;
                } 
                else i++;
            }
            if(num == 99)
               for (int x = 0; x < 9; x++)
                    used00[x] = 0;                    
        }
        private void insertIn01(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used01[i] == 0)
                {
                    used01[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used01[x] = 0;
        }
        private void insertIn02(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used02[i] == 0)
                {
                    used02[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used02[x] = 0;
        }
        private void insertIn03(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used03[i] == 0)
                {
                    used03[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used03[x] = 0;
        }
        private void insertIn04(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used04[i] == 0)
                {
                    used04[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used04[x] = 0;
        }
        private void insertIn05(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used05[i] == 0)
                {
                    used05[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used05[x] = 0;
        }
        private void insertIn06(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used06[i] == 0)
                {
                    used06[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used06[x] = 0;
        }
        private void insertIn07(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used07[i] == 0)
                {
                    used07[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used07[x] = 0;
        }
        private void insertIn08(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used08[i] == 0)
                {
                    used08[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used08[x] = 0;
        }

        private void insertIn10(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used10[i] == 0)
                {
                    used10[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used10[x] = 0;
        }
        private void insertIn11(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used11[i] == 0)
                {
                    used11[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used11[x] = 0;
        }
        private void insertIn12(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used12[i] == 0)
                {
                    used12[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used12[x] = 0;
        }
        private void insertIn13(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used13[i] == 0)
                {
                    used13[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used13[x] = 0;
        }
        private void insertIn14(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used14[i] == 0)
                {
                    used14[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used14[x] = 0;
        }
        private void insertIn15(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used15[i] == 0)
                {
                    used15[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used15[x] = 0;
        }
        private void insertIn16(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used16[i] == 0)
                {
                    used16[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used16[x] = 0;
        }
        private void insertIn17(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used17[i] == 0)
                {
                    used17[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used17[x] = 0;
        }
        private void insertIn18(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used18[i] == 0)
                {
                    used18[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used18[x] = 0;
        }

        private void insertIn20(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used20[i] == 0)
                {
                    used20[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used20[x] = 0;
        }
        private void insertIn21(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used21[i] == 0)
                {
                    used21[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used21[x] = 0;
        }
        private void insertIn22(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            { 
                if (used22[i] == 0)
                {
                    used22[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used22[x] = 0;
        }
        private void insertIn23(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used23[i] == 0)
                {
                    used23[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used23[x] = 0;
        }
        private void insertIn24(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used24[i] == 0)
                {
                    used24[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used24[x] = 0;
        }
        private void insertIn25(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used25[i] == 0)
                {
                    used25[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used25[x] = 0;
        }
        private void insertIn26(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used26[i] == 0)
                {
                    used26[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used26[x] = 0;
        }
        private void insertIn27(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used27[i] == 0)
                {
                    used27[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used27[x] = 0;
        }
        private void insertIn28(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used28[i] == 0)
                {
                    used28[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used28[x] = 0;
        }

        private void insertIn30(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used30[i] == 0)
                {
                    used30[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used30[x] = 0;
        }
        private void insertIn31(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used31[i] == 0)
                {
                    used31[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used31[x] = 0;
        }
        private void insertIn32(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used32[i] == 0)
                {
                    used32[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used32[x] = 0;
        }
        private void insertIn33(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used33[i] == 0)
                {
                    used33[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used33[x] = 0;
        }
        private void insertIn34(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used34[i] == 0)
                {
                    used34[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used34[x] = 0;
        }
        private void insertIn35(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used35[i] == 0)
                {
                    used35[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used35[x] = 0;
        }
        private void insertIn36(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used36[i] == 0)
                {
                    used36[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used36[x] = 0;
        }
        private void insertIn37(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used37[i] == 0)
                {
                    used37[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used37[x] = 0;
        }
        private void insertIn38(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used38[i] == 0)
                {
                    used38[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used38[x] = 0;
        }

        private void insertIn40(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used40[i] == 0)
                {
                    used40[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used40[x] = 0;
        }
        private void insertIn41(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used41[i] == 0)
                {
                    used41[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used41[x] = 0;
        }
        private void insertIn42(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used42[i] == 0)
                {
                    used42[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used42[x] = 0;
        }
        private void insertIn43(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used43[i] == 0)
                {
                    used43[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used43[x] = 0;
        }
        private void insertIn44(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used44[i] == 0)
                {
                    used44[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used44[x] = 0;
        }
        private void insertIn45(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used45[i] == 0)
                {
                    used45[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used45[x] = 0;
        }
        private void insertIn46(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used46[i] == 0)
                {
                    used46[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used46[x] = 0;
        }
        private void insertIn47(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used47[i] == 0)
                {
                    used47[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used47[x] = 0;
        }
        private void insertIn48(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used48[i] == 0)
                {
                    used48[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used48[x] = 0;
        }

        private void insertIn50(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used50[i] == 0)
                {
                    used50[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used50[x] = 0;
        }
        private void insertIn51(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used51[i] == 0)
                {
                    used51[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used51[x] = 0;
        }
        private void insertIn52(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used52[i] == 0)
                {
                    used52[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used52[x] = 0;
        }
        private void insertIn53(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used53[i] == 0)
                {
                    used53[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used53[x] = 0;
        }
        private void insertIn54(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used54[i] == 0)
                {
                    used54[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used54[x] = 0;
        }
        private void insertIn55(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used55[i] == 0)
                {
                    used55[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used55[x] = 0;
        }
        private void insertIn56(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used56[i] == 0)
                {
                    used56[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used56[x] = 0;
        }
        private void insertIn57(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used57[i] == 0)
                {
                    used57[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used57[x] = 0;
        }
        private void insertIn58(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used58[i] == 0)
                {
                    used58[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used58[x] = 0;
        }

        private void insertIn60(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used60[i] == 0)
                {
                    used60[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used60[x] = 0;
        }
        private void insertIn61(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used61[i] == 0)
                {
                    used61[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used61[x] = 0;
        }
        private void insertIn62(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used62[i] == 0)
                {
                    used62[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used62[x] = 0;
        }
        private void insertIn63(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used63[i] == 0)
                {
                    used63[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used63[x] = 0;
        }
        private void insertIn64(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used64[i] == 0)
                {
                    used64[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used64[x] = 0;
        }
        private void insertIn65(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used65[i] == 0)
                {
                    used65[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used65[x] = 0;
        }
        private void insertIn66(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used66[i] == 0)
                {
                    used66[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used66[x] = 0;
        }
        private void insertIn67(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used67[i] == 0)
                {
                    used67[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used67[x] = 0;
        }
        private void insertIn68(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used68[i] == 0)
                {
                    used68[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used68[x] = 0;
        }

        private void insertIn70(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used70[i] == 0)
                {
                    used70[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used70[x] = 0;
        }
        private void insertIn71(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used71[i] == 0)
                {
                    used71[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used71[x] = 0;
        }
        private void insertIn72(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used72[i] == 0)
                {
                    used72[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used72[x] = 0;
        }
        private void insertIn73(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used73[i] == 0)
                {
                    used73[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used73[x] = 0;
        }
        private void insertIn74(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used74[i] == 0)
                {
                    used74[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used74[x] = 0;
        }
        private void insertIn75(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used75[i] == 0)
                {
                    used75[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used75[x] = 0;
        }
        private void insertIn76(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used76[i] == 0)
                {
                    used76[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used76[x] = 0;
        }
        private void insertIn77(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used77[i] == 0)
                {
                    used77[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used77[x] = 0;
        }
        private void insertIn78(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used78[i] == 0)
                {
                    used78[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used78[x] = 0;
        }

        private void insertIn80(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used80[i] == 0)
                {
                    used80[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used80[x] = 0;
        }
        private void insertIn81(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used81[i] == 0)
                {
                    used81[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used81[x] = 0;
        }
        private void insertIn82(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used82[i] == 0)
                {
                    used82[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used82[x] = 0;
        }
        private void insertIn83(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used83[i] == 0)
                {
                    used83[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used83[x] = 0;
        }
        private void insertIn84(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used84[i] == 0)
                {
                    used84[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used84[x] = 0;
        }
        private void insertIn85(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used85[i] == 0)
                {
                    used85[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used85[x] = 0;
        }
        private void insertIn86(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used86[i] == 0)
                {
                    used86[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used86[x] = 0;
        }
        private void insertIn87(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used87[i] == 0)
                {
                    used87[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used87[x] = 0;
        }
        private void insertIn88(int num)
        {
            int i = 0;
            bool found = false;
            while (found == false)
            {
                if (used88[i] == 0)
                {
                    used88[i] = num;
                    found = true;
                }
                else i++;
            }
            if (num == 99)
                for (int x = 0; x < 9; x++)
                    used88[x] = 0;
        }


        private bool checkUsedIn00(int num)
       {
            for(int i = 0; i < 9; i++)           
                if (used00[i] == num) return true;
            return false;
       }
        private bool checkUsedIn01(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used01[i] == num) return true;
            return false;
        }
        private bool checkUsedIn02(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used02[i] == num) return true;
            return false;
        }
        private bool checkUsedIn03(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used03[i] == num) return true;
            return false;
        }
        private bool checkUsedIn04(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used04[i] == num) return true;
            return false;
        }
        private bool checkUsedIn05(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used05[i] == num) return true;
            return false;
        }
        private bool checkUsedIn06(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used06[i] == num) return true;
            return false;
        }
        private bool checkUsedIn07(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used07[i] == num) return true;
            return false;
        }
        private bool checkUsedIn08(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used08[i] == num) return true;
            return false;
        }

        private bool checkUsedIn10(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used10[i] == num) return true;
            return false;
        }
        private bool checkUsedIn11(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used11[i] == num) return true;
            return false;
        }
        private bool checkUsedIn12(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used12[i] == num) return true;
            return false;
        }
        private bool checkUsedIn13(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used13[i] == num) return true;
            return false;
        }
        private bool checkUsedIn14(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used14[i] == num) return true;
            return false;
        }
        private bool checkUsedIn15(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used15[i] == num) return true;
            return false;
        }
        private bool checkUsedIn16(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used16[i] == num) return true;
            return false;
        }
        private bool checkUsedIn17(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used17[i] == num) return true;
            return false;
        }
        private bool checkUsedIn18(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used18[i] == num) return true;
            return false;
        }

        private bool checkUsedIn20(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used20[i] == num) return true;
            return false;
        }
        private bool checkUsedIn21(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used21[i] == num) return true;
            return false;
        }
        private bool checkUsedIn22(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used22[i] == num) return true;
            return false;
        }
        private bool checkUsedIn23(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used23[i] == num) return true;
            return false;
        }
        private bool checkUsedIn24(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used24[i] == num) return true;
            return false;
        }
        private bool checkUsedIn25(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used25[i] == num) return true;
            return false;
        }
        private bool checkUsedIn26(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used26[i] == num) return true;
            return false;
        }
        private bool checkUsedIn27(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used27[i] == num) return true;
            return false;
        }
        private bool checkUsedIn28(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used28[i] == num) return true;
            return false;
        }

        private bool checkUsedIn30(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used30[i] == num) return true;
            return false;
        }
        private bool checkUsedIn31(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used31[i] == num) return true;
            return false;
        }
        private bool checkUsedIn32(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used32[i] == num) return true;
            return false;
        }
        private bool checkUsedIn33(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used33[i] == num) return true;
            return false;
        }
        private bool checkUsedIn34(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used34[i] == num) return true;
            return false;
        }
        private bool checkUsedIn35(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used35[i] == num) return true;
            return false;
        }
        private bool checkUsedIn36(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used36[i] == num) return true;
            return false;
        }
        private bool checkUsedIn37(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used37[i] == num) return true;
            return false;
        }
        private bool checkUsedIn38(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used38[i] == num) return true;
            return false;
        }

        private bool checkUsedIn40(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used40[i] == num) return true;
            return false;
        }
        private bool checkUsedIn41(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used41[i] == num) return true;
            return false;
        }
        private bool checkUsedIn42(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used42[i] == num) return true;
            return false;
        }
        private bool checkUsedIn43(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used43[i] == num) return true;
            return false;
        }
        private bool checkUsedIn44(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used44[i] == num) return true;
            return false;
        }
        private bool checkUsedIn45(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used45[i] == num) return true;
            return false;
        }
        private bool checkUsedIn46(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used46[i] == num) return true;
            return false;
        }
        private bool checkUsedIn47(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used47[i] == num) return true;
            return false;
        }
        private bool checkUsedIn48(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used48[i] == num) return true;
            return false;
        }

        private bool checkUsedIn50(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used50[i] == num) return true;
            return false;
        }
        private bool checkUsedIn51(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used51[i] == num) return true;
            return false;
        }
        private bool checkUsedIn52(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used52[i] == num) return true;
            return false;
        }
        private bool checkUsedIn53(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used53[i] == num) return true;
            return false;
        }
        private bool checkUsedIn54(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used54[i] == num) return true;
            return false;
        }
        private bool checkUsedIn55(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used55[i] == num) return true;
            return false;
        }
        private bool checkUsedIn56(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used56[i] == num) return true;
            return false;
        }
        private bool checkUsedIn57(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used57[i] == num) return true;
            return false;
        }
        private bool checkUsedIn58(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used58[i] == num) return true;
            return false;
        }

        private bool checkUsedIn60(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used60[i] == num) return true;
            return false;
        }
        private bool checkUsedIn61(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used61[i] == num) return true;
            return false;
        }
        private bool checkUsedIn62(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used62[i] == num) return true;
            return false;
        }
        private bool checkUsedIn63(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used63[i] == num) return true;
            return false;
        }
        private bool checkUsedIn64(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used64[i] == num) return true;
            return false;
        }
        private bool checkUsedIn65(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used65[i] == num) return true;
            return false;
        }
        private bool checkUsedIn66(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used66[i] == num) return true;
            return false;
        }
        private bool checkUsedIn67(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used67[i] == num) return true;
            return false;
        }
        private bool checkUsedIn68(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used68[i] == num) return true;
            return false;
        }

        private bool checkUsedIn70(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used70[i] == num) return true;
            return false;
        }
        private bool checkUsedIn71(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used71[i] == num) return true;
            return false;
        }
        private bool checkUsedIn72(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used72[i] == num) return true;
            return false;
        }
        private bool checkUsedIn73(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used73[i] == num) return true;
            return false;
        }
        private bool checkUsedIn74(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used74[i] == num) return true;
            return false;
        }
        private bool checkUsedIn75(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used75[i] == num) return true;
            return false;
        }
        private bool checkUsedIn76(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used76[i] == num) return true;
            return false;
        }
        private bool checkUsedIn77(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used77[i] == num) return true;
            return false;
        }
        private bool checkUsedIn78(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used78[i] == num) return true;
            return false;
        }

        private bool checkUsedIn80(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used80[i] == num) return true;
            return false;
        }
        private bool checkUsedIn81(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used81[i] == num) return true;
            return false;
        }
        private bool checkUsedIn82(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used82[i] == num) return true;
            return false;
        }
        private bool checkUsedIn83(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used83[i] == num) return true;
            return false;
        }
        private bool checkUsedIn84(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used84[i] == num) return true;
            return false;
        }
        private bool checkUsedIn85(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used85[i] == num) return true;
            return false;
        }
        private bool checkUsedIn86(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used86[i] == num) return true;
            return false;
        }
        private bool checkUsedIn87(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used87[i] == num) return true;
            return false;
        }
        private bool checkUsedIn88(int num)
        {
            for (int i = 0; i < 9; i++)
                if (used88[i] == num) return true;
            return false;
        }

        private void FillMainMatrix()
        {
            //ako je taj broj fiksan, tj unaprijed upisan
            if (matrixFixed[grow, gcolumn] != 0)
            {
                if(goingBack == true)
                {
                    gcolumn--;
                }
                else
                {
                    gcolumn++;
                }
              
                if(gcolumn == 9)
                {
                    grow++;
                    gcolumn = 0;                   
                }
                else if(gcolumn == -1)
                {
                    grow--;
                    gcolumn = 8;
                }
                if (grow == 9)
                {
                    success = true;
                    end = true;
                }
                else if(grow == -1)
                {
                    success = false;
                    end = true;
                }  
                
            }
            //ako je polje prazno, treba upisati broj u njega...
            else
            {
                eraseArrayOfPos();
                goingBack = false;

                for(int i = 1; i <= 9; i++)
                {
                    //broj se dodaje kao mogući broj 
                    if(FindInRow(grow,i) == false && FindInColumn(gcolumn,i) == false && FindInSquare(grow,gcolumn,i) == false && isUsed(grow,gcolumn,i) == false)
                    {
                        arrayOfPos[countInArrayOfPos] = i;
                        countInArrayOfPos++;
                    }
                }
                //ako nema nijedan broj koji moze ici u to polje, vrati se nazad jedno
                if(countInArrayOfPos == 0)
                {
                    //obrisi koristene brojeve prije nego se vratis korak unazad
                    insertIntoUsed(grow, gcolumn, 99);
                    matrixMain[grow, gcolumn] = 0;

                    gcolumn--;
                    if(gcolumn == -1)
                    {
                        gcolumn = 8;
                        grow--;                        
                    }
                    if (grow == -1)
                    {
                        success = false;
                        end = true;
                    }
                    goingBack = true;
                    
                }
                //ako postoji neki broj koji bi se mogao upisati, upisi ga u matricu
                else
                {
                    matrixMain[grow, gcolumn] = arrayOfPos[0];
                    insertIntoUsed(grow, gcolumn, arrayOfPos[0]);
                    printInTextBoxes(grow);

                    gcolumn++;
                    if(gcolumn == 9)
                    {
                        grow++;
                        gcolumn = 0;                     
                    }
                    if (grow == 9)
                    {
                        success = true;
                        end = true;
                    }
                   
                }

            }

            

            
        }


        private bool FindInRow(int row,int randomNum)
        {
            for(int column = 0; column < 9; column++)
            {
                if(matrixMain[row, column] == randomNum)
                {
                    return true;
                }                
            }
            return false;
        }
        private bool FindInColumn(int column, int randomNum)
        {
            for (int row = 0; row < 9; row++)
            {
                if (matrixMain[row, column] == randomNum)
                {
                    return true;
                }
            }
            return false;
        }
        private bool FindInSquare(int row, int column, int randomNum)
        {           
            int rowdb, columndb, rowub, columnub;

            if (row < 3 && column < 3)
            {               
                rowdb = 0;
                columndb = 0;
                rowub = 2;
                columnub = 2;
            }
            else if (row < 3 && column < 6)
            {                
                rowdb = 0;
                columndb = 3;
                rowub = 2;
                columnub = 5;
            }
            else if (row < 3 && column < 9)
            {              
                rowdb = 0;
                columndb = 6;
                rowub = 2;
                columnub = 8;
            }
            else if (row < 6 && column < 3)
            {
                rowdb = 3;
                columndb = 0;
                rowub = 5;
                columnub = 2;
            }
            else if (row < 6 && column < 6)
            {               
                rowdb = 3;
                columndb = 3;
                rowub = 5;
                columnub = 5;
            }
            else if (row < 6 && column < 9)
            {                
                rowdb = 3;
                columndb = 6;
                rowub = 5;
                columnub = 8;
            }
            else if (row < 9 && column < 3)
            {               
                rowdb = 6;
                columndb = 0;
                rowub = 8;
                columnub = 2;
            }
            else if (row < 9 && column < 6)
            {                
                rowdb = 6;
                columndb = 3;
                rowub = 8;
                columnub = 5;
            }
            else
            {                
                rowdb = 6;
                columndb = 6;
                rowub = 8;
                columnub = 8;
            }


            for (int x = rowdb; x <= rowub; x++)
            {
                for (int y = columndb; y <= columnub; y++)
                {
                    if (matrixMain[x,y] == randomNum) return true;
                }
            }
            return false;
        }

        private void printInTextBoxes(int row)
        {



            if (row == 0)
            {
                tb00.Text = matrixMain[0, 0].ToString();
                tb01.Text = matrixMain[0, 1].ToString();
                tb02.Text = matrixMain[0, 2].ToString();
                tb03.Text = matrixMain[0, 3].ToString();
                tb04.Text = matrixMain[0, 4].ToString();
                tb05.Text = matrixMain[0, 5].ToString();
                tb06.Text = matrixMain[0, 6].ToString();
                tb07.Text = matrixMain[0, 7].ToString();
                tb08.Text = matrixMain[0, 8].ToString();
            }
            else if (row == 1)
            {
                tb10.Text = matrixMain[1, 0].ToString();
                tb11.Text = matrixMain[1, 1].ToString();
                tb12.Text = matrixMain[1, 2].ToString();
                tb13.Text = matrixMain[1, 3].ToString();
                tb14.Text = matrixMain[1, 4].ToString();
                tb15.Text = matrixMain[1, 5].ToString();
                tb16.Text = matrixMain[1, 6].ToString();
                tb17.Text = matrixMain[1, 7].ToString();
                tb18.Text = matrixMain[1, 8].ToString();
            }
            else if (row == 2)
            {
                tb20.Text = matrixMain[2, 0].ToString();
                tb21.Text = matrixMain[2, 1].ToString();
                tb22.Text = matrixMain[2, 2].ToString();
                tb23.Text = matrixMain[2, 3].ToString();
                tb24.Text = matrixMain[2, 4].ToString();
                tb25.Text = matrixMain[2, 5].ToString();
                tb26.Text = matrixMain[2, 6].ToString();
                tb27.Text = matrixMain[2, 7].ToString();
                tb28.Text = matrixMain[2, 8].ToString();
            }
            else if (row == 3)
            {
                tb30.Text = matrixMain[3, 0].ToString();
                tb31.Text = matrixMain[3, 1].ToString();
                tb32.Text = matrixMain[3, 2].ToString();
                tb33.Text = matrixMain[3, 3].ToString();
                tb34.Text = matrixMain[3, 4].ToString();
                tb35.Text = matrixMain[3, 5].ToString();
                tb36.Text = matrixMain[3, 6].ToString();
                tb37.Text = matrixMain[3, 7].ToString();
                tb38.Text = matrixMain[3, 8].ToString();
            }
            else if (row == 4)
            {
                tb40.Text = matrixMain[4, 0].ToString();
                tb41.Text = matrixMain[4, 1].ToString();
                tb42.Text = matrixMain[4, 2].ToString();
                tb43.Text = matrixMain[4, 3].ToString();
                tb44.Text = matrixMain[4, 4].ToString();
                tb45.Text = matrixMain[4, 5].ToString();
                tb46.Text = matrixMain[4, 6].ToString();
                tb47.Text = matrixMain[4, 7].ToString();
                tb48.Text = matrixMain[4, 8].ToString();
            }
            else if (row == 5)
            {
                tb50.Text = matrixMain[5, 0].ToString();
                tb51.Text = matrixMain[5, 1].ToString();
                tb52.Text = matrixMain[5, 2].ToString();
                tb53.Text = matrixMain[5, 3].ToString();
                tb54.Text = matrixMain[5, 4].ToString();
                tb55.Text = matrixMain[5, 5].ToString();
                tb56.Text = matrixMain[5, 6].ToString();
                tb57.Text = matrixMain[5, 7].ToString();
                tb58.Text = matrixMain[5, 8].ToString();
            }
            else if (row == 6)
            {
                tb60.Text = matrixMain[6, 0].ToString();
                tb61.Text = matrixMain[6, 1].ToString();
                tb62.Text = matrixMain[6, 2].ToString();
                tb63.Text = matrixMain[6, 3].ToString();
                tb64.Text = matrixMain[6, 4].ToString();
                tb65.Text = matrixMain[6, 5].ToString();
                tb66.Text = matrixMain[6, 6].ToString();
                tb67.Text = matrixMain[6, 7].ToString();
                tb68.Text = matrixMain[6, 8].ToString();
            }
            else if (row == 7)
            {
                tb70.Text = matrixMain[7, 0].ToString();
                tb71.Text = matrixMain[7, 1].ToString();
                tb72.Text = matrixMain[7, 2].ToString();
                tb73.Text = matrixMain[7, 3].ToString();
                tb74.Text = matrixMain[7, 4].ToString();
                tb75.Text = matrixMain[7, 5].ToString();
                tb76.Text = matrixMain[7, 6].ToString();
                tb77.Text = matrixMain[7, 7].ToString();
                tb78.Text = matrixMain[7, 8].ToString();
            }
            else if (row == 8)
            {
                tb80.Text = matrixMain[8, 0].ToString();
                tb81.Text = matrixMain[8, 1].ToString();
                tb82.Text = matrixMain[8, 2].ToString();
                tb83.Text = matrixMain[8, 3].ToString();
                tb84.Text = matrixMain[8, 4].ToString();
                tb85.Text = matrixMain[8, 5].ToString();
                tb86.Text = matrixMain[8, 6].ToString();
                tb87.Text = matrixMain[8, 7].ToString();
                tb88.Text = matrixMain[8, 8].ToString();
            }

        }

        private int getRandomNum(int dg, int gg)
        {
            return rnd.Next(dg, gg + 1);
        }
        
        public void solveSudoku()
        {
            try
            {
                LoadInMatrix();
            }
            catch (Exception)
            {
                end = true;
                MessageBox.Show("Krivi unos.");
            }
            

            swatch.Start();
            btnStart.Enabled = false;
            int steps = 0;
            while (end == false)
            {
                FillMainMatrix();
                progressBar1.Value = grow * 10 + 10;
                steps++;       
            }

            swatch.Stop();
            if (success)
            {
                lb_notify.Text = "Uspješno riješen sudoku ... [ "+swatch.Elapsed.ToString()+" ] ... [ "+steps.ToString()+" ]";

                for (int i = 0; i < 9; i++)
                    printInTextBoxes(i);
            }
            else
            {
                lb_notify.Text = "Ne mogu riješiti ovaj sudoku ...##... Vrijeme : " + swatch.Elapsed.ToString();
            }

          
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            solveSudoku();
        }
        
        
    }
}
