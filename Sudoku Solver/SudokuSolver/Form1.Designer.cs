﻿namespace SudokuSolver
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb00 = new System.Windows.Forms.RichTextBox();
            this.tb01 = new System.Windows.Forms.RichTextBox();
            this.tb02 = new System.Windows.Forms.RichTextBox();
            this.tb03 = new System.Windows.Forms.RichTextBox();
            this.tb04 = new System.Windows.Forms.RichTextBox();
            this.tb05 = new System.Windows.Forms.RichTextBox();
            this.tb06 = new System.Windows.Forms.RichTextBox();
            this.tb07 = new System.Windows.Forms.RichTextBox();
            this.tb08 = new System.Windows.Forms.RichTextBox();
            this.tb10 = new System.Windows.Forms.RichTextBox();
            this.tb11 = new System.Windows.Forms.RichTextBox();
            this.tb12 = new System.Windows.Forms.RichTextBox();
            this.tb13 = new System.Windows.Forms.RichTextBox();
            this.tb14 = new System.Windows.Forms.RichTextBox();
            this.tb15 = new System.Windows.Forms.RichTextBox();
            this.tb18 = new System.Windows.Forms.RichTextBox();
            this.tb16 = new System.Windows.Forms.RichTextBox();
            this.tb17 = new System.Windows.Forms.RichTextBox();
            this.tb20 = new System.Windows.Forms.RichTextBox();
            this.tb21 = new System.Windows.Forms.RichTextBox();
            this.tb22 = new System.Windows.Forms.RichTextBox();
            this.tb23 = new System.Windows.Forms.RichTextBox();
            this.tb24 = new System.Windows.Forms.RichTextBox();
            this.tb25 = new System.Windows.Forms.RichTextBox();
            this.tb28 = new System.Windows.Forms.RichTextBox();
            this.tb26 = new System.Windows.Forms.RichTextBox();
            this.tb27 = new System.Windows.Forms.RichTextBox();
            this.tb30 = new System.Windows.Forms.RichTextBox();
            this.tb31 = new System.Windows.Forms.RichTextBox();
            this.tb32 = new System.Windows.Forms.RichTextBox();
            this.tb33 = new System.Windows.Forms.RichTextBox();
            this.tb34 = new System.Windows.Forms.RichTextBox();
            this.tb35 = new System.Windows.Forms.RichTextBox();
            this.tb38 = new System.Windows.Forms.RichTextBox();
            this.tb36 = new System.Windows.Forms.RichTextBox();
            this.tb37 = new System.Windows.Forms.RichTextBox();
            this.tb40 = new System.Windows.Forms.RichTextBox();
            this.tb41 = new System.Windows.Forms.RichTextBox();
            this.tb42 = new System.Windows.Forms.RichTextBox();
            this.tb43 = new System.Windows.Forms.RichTextBox();
            this.tb44 = new System.Windows.Forms.RichTextBox();
            this.tb45 = new System.Windows.Forms.RichTextBox();
            this.tb48 = new System.Windows.Forms.RichTextBox();
            this.tb46 = new System.Windows.Forms.RichTextBox();
            this.tb47 = new System.Windows.Forms.RichTextBox();
            this.tb50 = new System.Windows.Forms.RichTextBox();
            this.tb51 = new System.Windows.Forms.RichTextBox();
            this.tb52 = new System.Windows.Forms.RichTextBox();
            this.tb53 = new System.Windows.Forms.RichTextBox();
            this.tb54 = new System.Windows.Forms.RichTextBox();
            this.tb55 = new System.Windows.Forms.RichTextBox();
            this.tb58 = new System.Windows.Forms.RichTextBox();
            this.tb56 = new System.Windows.Forms.RichTextBox();
            this.tb57 = new System.Windows.Forms.RichTextBox();
            this.tb60 = new System.Windows.Forms.RichTextBox();
            this.tb61 = new System.Windows.Forms.RichTextBox();
            this.tb62 = new System.Windows.Forms.RichTextBox();
            this.tb63 = new System.Windows.Forms.RichTextBox();
            this.tb64 = new System.Windows.Forms.RichTextBox();
            this.tb65 = new System.Windows.Forms.RichTextBox();
            this.tb68 = new System.Windows.Forms.RichTextBox();
            this.tb66 = new System.Windows.Forms.RichTextBox();
            this.tb67 = new System.Windows.Forms.RichTextBox();
            this.tb70 = new System.Windows.Forms.RichTextBox();
            this.tb71 = new System.Windows.Forms.RichTextBox();
            this.tb72 = new System.Windows.Forms.RichTextBox();
            this.tb73 = new System.Windows.Forms.RichTextBox();
            this.tb74 = new System.Windows.Forms.RichTextBox();
            this.tb75 = new System.Windows.Forms.RichTextBox();
            this.tb78 = new System.Windows.Forms.RichTextBox();
            this.tb76 = new System.Windows.Forms.RichTextBox();
            this.tb77 = new System.Windows.Forms.RichTextBox();
            this.tb80 = new System.Windows.Forms.RichTextBox();
            this.tb81 = new System.Windows.Forms.RichTextBox();
            this.tb82 = new System.Windows.Forms.RichTextBox();
            this.tb83 = new System.Windows.Forms.RichTextBox();
            this.tb84 = new System.Windows.Forms.RichTextBox();
            this.tb85 = new System.Windows.Forms.RichTextBox();
            this.tb88 = new System.Windows.Forms.RichTextBox();
            this.tb86 = new System.Windows.Forms.RichTextBox();
            this.tb87 = new System.Windows.Forms.RichTextBox();
            this.btnStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.lb_notify = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb00
            // 
            this.tb00.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb00.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb00.Location = new System.Drawing.Point(29, 30);
            this.tb00.Margin = new System.Windows.Forms.Padding(0);
            this.tb00.MaxLength = 1;
            this.tb00.Multiline = false;
            this.tb00.Name = "tb00";
            this.tb00.Size = new System.Drawing.Size(45, 45);
            this.tb00.TabIndex = 0;
            this.tb00.Text = "";
            // 
            // tb01
            // 
            this.tb01.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb01.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb01.Location = new System.Drawing.Point(74, 30);
            this.tb01.Margin = new System.Windows.Forms.Padding(0);
            this.tb01.MaxLength = 1;
            this.tb01.Multiline = false;
            this.tb01.Name = "tb01";
            this.tb01.Size = new System.Drawing.Size(45, 45);
            this.tb01.TabIndex = 1;
            this.tb01.Text = "";
            // 
            // tb02
            // 
            this.tb02.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb02.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb02.Location = new System.Drawing.Point(119, 30);
            this.tb02.Margin = new System.Windows.Forms.Padding(0);
            this.tb02.MaxLength = 1;
            this.tb02.Multiline = false;
            this.tb02.Name = "tb02";
            this.tb02.Size = new System.Drawing.Size(45, 45);
            this.tb02.TabIndex = 2;
            this.tb02.Text = "";
            // 
            // tb03
            // 
            this.tb03.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb03.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb03.Location = new System.Drawing.Point(172, 30);
            this.tb03.Margin = new System.Windows.Forms.Padding(0);
            this.tb03.MaxLength = 1;
            this.tb03.Multiline = false;
            this.tb03.Name = "tb03";
            this.tb03.Size = new System.Drawing.Size(45, 45);
            this.tb03.TabIndex = 3;
            this.tb03.Text = "";
            // 
            // tb04
            // 
            this.tb04.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb04.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb04.Location = new System.Drawing.Point(217, 30);
            this.tb04.Margin = new System.Windows.Forms.Padding(0);
            this.tb04.MaxLength = 1;
            this.tb04.Multiline = false;
            this.tb04.Name = "tb04";
            this.tb04.Size = new System.Drawing.Size(45, 45);
            this.tb04.TabIndex = 4;
            this.tb04.Text = "";
            // 
            // tb05
            // 
            this.tb05.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb05.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb05.Location = new System.Drawing.Point(262, 30);
            this.tb05.Margin = new System.Windows.Forms.Padding(0);
            this.tb05.MaxLength = 1;
            this.tb05.Multiline = false;
            this.tb05.Name = "tb05";
            this.tb05.Size = new System.Drawing.Size(45, 45);
            this.tb05.TabIndex = 5;
            this.tb05.Text = "";
            // 
            // tb06
            // 
            this.tb06.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb06.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb06.Location = new System.Drawing.Point(317, 30);
            this.tb06.Margin = new System.Windows.Forms.Padding(0);
            this.tb06.MaxLength = 1;
            this.tb06.Multiline = false;
            this.tb06.Name = "tb06";
            this.tb06.Size = new System.Drawing.Size(45, 45);
            this.tb06.TabIndex = 6;
            this.tb06.Text = "";
            // 
            // tb07
            // 
            this.tb07.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb07.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb07.Location = new System.Drawing.Point(362, 30);
            this.tb07.Margin = new System.Windows.Forms.Padding(0);
            this.tb07.MaxLength = 1;
            this.tb07.Multiline = false;
            this.tb07.Name = "tb07";
            this.tb07.Size = new System.Drawing.Size(45, 45);
            this.tb07.TabIndex = 7;
            this.tb07.Text = "";
            // 
            // tb08
            // 
            this.tb08.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb08.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb08.Location = new System.Drawing.Point(407, 30);
            this.tb08.Margin = new System.Windows.Forms.Padding(0);
            this.tb08.MaxLength = 1;
            this.tb08.Multiline = false;
            this.tb08.Name = "tb08";
            this.tb08.Size = new System.Drawing.Size(45, 45);
            this.tb08.TabIndex = 8;
            this.tb08.Text = "";
            // 
            // tb10
            // 
            this.tb10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb10.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb10.Location = new System.Drawing.Point(29, 76);
            this.tb10.Margin = new System.Windows.Forms.Padding(0);
            this.tb10.MaxLength = 1;
            this.tb10.Multiline = false;
            this.tb10.Name = "tb10";
            this.tb10.Size = new System.Drawing.Size(45, 45);
            this.tb10.TabIndex = 9;
            this.tb10.Text = "";
            // 
            // tb11
            // 
            this.tb11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb11.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb11.Location = new System.Drawing.Point(74, 76);
            this.tb11.Margin = new System.Windows.Forms.Padding(0);
            this.tb11.MaxLength = 1;
            this.tb11.Multiline = false;
            this.tb11.Name = "tb11";
            this.tb11.Size = new System.Drawing.Size(45, 45);
            this.tb11.TabIndex = 10;
            this.tb11.Text = "";
            // 
            // tb12
            // 
            this.tb12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb12.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb12.Location = new System.Drawing.Point(119, 76);
            this.tb12.Margin = new System.Windows.Forms.Padding(0);
            this.tb12.MaxLength = 1;
            this.tb12.Multiline = false;
            this.tb12.Name = "tb12";
            this.tb12.Size = new System.Drawing.Size(45, 45);
            this.tb12.TabIndex = 11;
            this.tb12.Text = "";
            // 
            // tb13
            // 
            this.tb13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb13.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb13.Location = new System.Drawing.Point(172, 76);
            this.tb13.Margin = new System.Windows.Forms.Padding(0);
            this.tb13.MaxLength = 1;
            this.tb13.Multiline = false;
            this.tb13.Name = "tb13";
            this.tb13.Size = new System.Drawing.Size(45, 45);
            this.tb13.TabIndex = 12;
            this.tb13.Text = "";
            // 
            // tb14
            // 
            this.tb14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb14.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb14.Location = new System.Drawing.Point(217, 76);
            this.tb14.Margin = new System.Windows.Forms.Padding(0);
            this.tb14.MaxLength = 1;
            this.tb14.Multiline = false;
            this.tb14.Name = "tb14";
            this.tb14.Size = new System.Drawing.Size(45, 45);
            this.tb14.TabIndex = 13;
            this.tb14.Text = "";
            // 
            // tb15
            // 
            this.tb15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb15.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb15.Location = new System.Drawing.Point(262, 76);
            this.tb15.Margin = new System.Windows.Forms.Padding(0);
            this.tb15.MaxLength = 1;
            this.tb15.Multiline = false;
            this.tb15.Name = "tb15";
            this.tb15.Size = new System.Drawing.Size(45, 45);
            this.tb15.TabIndex = 14;
            this.tb15.Text = "";
            // 
            // tb18
            // 
            this.tb18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb18.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb18.Location = new System.Drawing.Point(407, 76);
            this.tb18.Margin = new System.Windows.Forms.Padding(0);
            this.tb18.MaxLength = 1;
            this.tb18.Multiline = false;
            this.tb18.Name = "tb18";
            this.tb18.Size = new System.Drawing.Size(45, 45);
            this.tb18.TabIndex = 17;
            this.tb18.Text = "";
            // 
            // tb16
            // 
            this.tb16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb16.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb16.Location = new System.Drawing.Point(317, 76);
            this.tb16.Margin = new System.Windows.Forms.Padding(0);
            this.tb16.MaxLength = 1;
            this.tb16.Multiline = false;
            this.tb16.Name = "tb16";
            this.tb16.Size = new System.Drawing.Size(45, 45);
            this.tb16.TabIndex = 15;
            this.tb16.Text = "";
            // 
            // tb17
            // 
            this.tb17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb17.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb17.Location = new System.Drawing.Point(362, 76);
            this.tb17.Margin = new System.Windows.Forms.Padding(0);
            this.tb17.MaxLength = 1;
            this.tb17.Multiline = false;
            this.tb17.Name = "tb17";
            this.tb17.Size = new System.Drawing.Size(45, 45);
            this.tb17.TabIndex = 16;
            this.tb17.Text = "";
            // 
            // tb20
            // 
            this.tb20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb20.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb20.Location = new System.Drawing.Point(29, 122);
            this.tb20.Margin = new System.Windows.Forms.Padding(0);
            this.tb20.MaxLength = 1;
            this.tb20.Multiline = false;
            this.tb20.Name = "tb20";
            this.tb20.Size = new System.Drawing.Size(45, 45);
            this.tb20.TabIndex = 18;
            this.tb20.Text = "";
            // 
            // tb21
            // 
            this.tb21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb21.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb21.Location = new System.Drawing.Point(74, 122);
            this.tb21.Margin = new System.Windows.Forms.Padding(0);
            this.tb21.MaxLength = 1;
            this.tb21.Multiline = false;
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(45, 45);
            this.tb21.TabIndex = 19;
            this.tb21.Text = "";
            // 
            // tb22
            // 
            this.tb22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb22.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb22.Location = new System.Drawing.Point(119, 122);
            this.tb22.Margin = new System.Windows.Forms.Padding(0);
            this.tb22.MaxLength = 1;
            this.tb22.Multiline = false;
            this.tb22.Name = "tb22";
            this.tb22.Size = new System.Drawing.Size(45, 45);
            this.tb22.TabIndex = 20;
            this.tb22.Text = "";
            // 
            // tb23
            // 
            this.tb23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb23.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb23.Location = new System.Drawing.Point(172, 122);
            this.tb23.Margin = new System.Windows.Forms.Padding(0);
            this.tb23.MaxLength = 1;
            this.tb23.Multiline = false;
            this.tb23.Name = "tb23";
            this.tb23.Size = new System.Drawing.Size(45, 45);
            this.tb23.TabIndex = 21;
            this.tb23.Text = "";
            // 
            // tb24
            // 
            this.tb24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb24.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb24.Location = new System.Drawing.Point(217, 122);
            this.tb24.Margin = new System.Windows.Forms.Padding(0);
            this.tb24.MaxLength = 1;
            this.tb24.Multiline = false;
            this.tb24.Name = "tb24";
            this.tb24.Size = new System.Drawing.Size(45, 45);
            this.tb24.TabIndex = 22;
            this.tb24.Text = "";
            // 
            // tb25
            // 
            this.tb25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb25.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb25.Location = new System.Drawing.Point(262, 122);
            this.tb25.Margin = new System.Windows.Forms.Padding(0);
            this.tb25.MaxLength = 1;
            this.tb25.Multiline = false;
            this.tb25.Name = "tb25";
            this.tb25.Size = new System.Drawing.Size(45, 45);
            this.tb25.TabIndex = 23;
            this.tb25.Text = "";
            // 
            // tb28
            // 
            this.tb28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb28.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb28.Location = new System.Drawing.Point(407, 122);
            this.tb28.Margin = new System.Windows.Forms.Padding(0);
            this.tb28.MaxLength = 1;
            this.tb28.Multiline = false;
            this.tb28.Name = "tb28";
            this.tb28.Size = new System.Drawing.Size(45, 45);
            this.tb28.TabIndex = 26;
            this.tb28.Text = "";
            // 
            // tb26
            // 
            this.tb26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb26.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb26.Location = new System.Drawing.Point(317, 122);
            this.tb26.Margin = new System.Windows.Forms.Padding(0);
            this.tb26.MaxLength = 1;
            this.tb26.Multiline = false;
            this.tb26.Name = "tb26";
            this.tb26.Size = new System.Drawing.Size(45, 45);
            this.tb26.TabIndex = 24;
            this.tb26.Text = "";
            // 
            // tb27
            // 
            this.tb27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb27.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb27.Location = new System.Drawing.Point(362, 122);
            this.tb27.Margin = new System.Windows.Forms.Padding(0);
            this.tb27.MaxLength = 1;
            this.tb27.Multiline = false;
            this.tb27.Name = "tb27";
            this.tb27.Size = new System.Drawing.Size(45, 45);
            this.tb27.TabIndex = 25;
            this.tb27.Text = "";
            // 
            // tb30
            // 
            this.tb30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb30.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb30.Location = new System.Drawing.Point(29, 172);
            this.tb30.Margin = new System.Windows.Forms.Padding(0);
            this.tb30.MaxLength = 1;
            this.tb30.Multiline = false;
            this.tb30.Name = "tb30";
            this.tb30.Size = new System.Drawing.Size(45, 45);
            this.tb30.TabIndex = 27;
            this.tb30.Text = "";
            // 
            // tb31
            // 
            this.tb31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb31.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb31.Location = new System.Drawing.Point(74, 172);
            this.tb31.Margin = new System.Windows.Forms.Padding(0);
            this.tb31.MaxLength = 1;
            this.tb31.Multiline = false;
            this.tb31.Name = "tb31";
            this.tb31.Size = new System.Drawing.Size(45, 45);
            this.tb31.TabIndex = 28;
            this.tb31.Text = "";
            // 
            // tb32
            // 
            this.tb32.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb32.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb32.Location = new System.Drawing.Point(119, 172);
            this.tb32.Margin = new System.Windows.Forms.Padding(0);
            this.tb32.MaxLength = 1;
            this.tb32.Multiline = false;
            this.tb32.Name = "tb32";
            this.tb32.Size = new System.Drawing.Size(45, 45);
            this.tb32.TabIndex = 29;
            this.tb32.Text = "";
            // 
            // tb33
            // 
            this.tb33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb33.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb33.Location = new System.Drawing.Point(172, 172);
            this.tb33.Margin = new System.Windows.Forms.Padding(0);
            this.tb33.MaxLength = 1;
            this.tb33.Multiline = false;
            this.tb33.Name = "tb33";
            this.tb33.Size = new System.Drawing.Size(45, 45);
            this.tb33.TabIndex = 30;
            this.tb33.Text = "";
            // 
            // tb34
            // 
            this.tb34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb34.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb34.Location = new System.Drawing.Point(217, 172);
            this.tb34.Margin = new System.Windows.Forms.Padding(0);
            this.tb34.MaxLength = 1;
            this.tb34.Multiline = false;
            this.tb34.Name = "tb34";
            this.tb34.Size = new System.Drawing.Size(45, 45);
            this.tb34.TabIndex = 31;
            this.tb34.Text = "";
            // 
            // tb35
            // 
            this.tb35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb35.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb35.Location = new System.Drawing.Point(262, 172);
            this.tb35.Margin = new System.Windows.Forms.Padding(0);
            this.tb35.MaxLength = 1;
            this.tb35.Multiline = false;
            this.tb35.Name = "tb35";
            this.tb35.Size = new System.Drawing.Size(45, 45);
            this.tb35.TabIndex = 32;
            this.tb35.Text = "";
            // 
            // tb38
            // 
            this.tb38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb38.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb38.Location = new System.Drawing.Point(407, 172);
            this.tb38.Margin = new System.Windows.Forms.Padding(0);
            this.tb38.MaxLength = 1;
            this.tb38.Multiline = false;
            this.tb38.Name = "tb38";
            this.tb38.Size = new System.Drawing.Size(45, 45);
            this.tb38.TabIndex = 35;
            this.tb38.Text = "";
            // 
            // tb36
            // 
            this.tb36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb36.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb36.Location = new System.Drawing.Point(317, 172);
            this.tb36.Margin = new System.Windows.Forms.Padding(0);
            this.tb36.MaxLength = 1;
            this.tb36.Multiline = false;
            this.tb36.Name = "tb36";
            this.tb36.Size = new System.Drawing.Size(45, 45);
            this.tb36.TabIndex = 33;
            this.tb36.Text = "";
            // 
            // tb37
            // 
            this.tb37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb37.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb37.Location = new System.Drawing.Point(362, 172);
            this.tb37.Margin = new System.Windows.Forms.Padding(0);
            this.tb37.MaxLength = 1;
            this.tb37.Multiline = false;
            this.tb37.Name = "tb37";
            this.tb37.Size = new System.Drawing.Size(45, 45);
            this.tb37.TabIndex = 34;
            this.tb37.Text = "";
            // 
            // tb40
            // 
            this.tb40.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb40.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb40.Location = new System.Drawing.Point(29, 218);
            this.tb40.Margin = new System.Windows.Forms.Padding(0);
            this.tb40.MaxLength = 1;
            this.tb40.Multiline = false;
            this.tb40.Name = "tb40";
            this.tb40.Size = new System.Drawing.Size(45, 45);
            this.tb40.TabIndex = 36;
            this.tb40.Text = "";
            // 
            // tb41
            // 
            this.tb41.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb41.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb41.Location = new System.Drawing.Point(74, 218);
            this.tb41.Margin = new System.Windows.Forms.Padding(0);
            this.tb41.MaxLength = 1;
            this.tb41.Multiline = false;
            this.tb41.Name = "tb41";
            this.tb41.Size = new System.Drawing.Size(45, 45);
            this.tb41.TabIndex = 37;
            this.tb41.Text = "";
            // 
            // tb42
            // 
            this.tb42.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb42.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb42.Location = new System.Drawing.Point(119, 218);
            this.tb42.Margin = new System.Windows.Forms.Padding(0);
            this.tb42.MaxLength = 1;
            this.tb42.Multiline = false;
            this.tb42.Name = "tb42";
            this.tb42.Size = new System.Drawing.Size(45, 45);
            this.tb42.TabIndex = 38;
            this.tb42.Text = "";
            // 
            // tb43
            // 
            this.tb43.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb43.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb43.Location = new System.Drawing.Point(172, 218);
            this.tb43.Margin = new System.Windows.Forms.Padding(0);
            this.tb43.MaxLength = 1;
            this.tb43.Multiline = false;
            this.tb43.Name = "tb43";
            this.tb43.Size = new System.Drawing.Size(45, 45);
            this.tb43.TabIndex = 39;
            this.tb43.Text = "";
            // 
            // tb44
            // 
            this.tb44.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb44.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb44.Location = new System.Drawing.Point(217, 218);
            this.tb44.Margin = new System.Windows.Forms.Padding(0);
            this.tb44.MaxLength = 1;
            this.tb44.Multiline = false;
            this.tb44.Name = "tb44";
            this.tb44.Size = new System.Drawing.Size(45, 45);
            this.tb44.TabIndex = 40;
            this.tb44.Text = "";
            // 
            // tb45
            // 
            this.tb45.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb45.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb45.Location = new System.Drawing.Point(262, 218);
            this.tb45.Margin = new System.Windows.Forms.Padding(0);
            this.tb45.MaxLength = 1;
            this.tb45.Multiline = false;
            this.tb45.Name = "tb45";
            this.tb45.Size = new System.Drawing.Size(45, 45);
            this.tb45.TabIndex = 41;
            this.tb45.Text = "";
            // 
            // tb48
            // 
            this.tb48.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb48.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb48.Location = new System.Drawing.Point(407, 218);
            this.tb48.Margin = new System.Windows.Forms.Padding(0);
            this.tb48.MaxLength = 1;
            this.tb48.Multiline = false;
            this.tb48.Name = "tb48";
            this.tb48.Size = new System.Drawing.Size(45, 45);
            this.tb48.TabIndex = 44;
            this.tb48.Text = "";
            // 
            // tb46
            // 
            this.tb46.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb46.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb46.Location = new System.Drawing.Point(317, 218);
            this.tb46.Margin = new System.Windows.Forms.Padding(0);
            this.tb46.MaxLength = 1;
            this.tb46.Multiline = false;
            this.tb46.Name = "tb46";
            this.tb46.Size = new System.Drawing.Size(45, 45);
            this.tb46.TabIndex = 42;
            this.tb46.Text = "";
            // 
            // tb47
            // 
            this.tb47.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb47.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb47.Location = new System.Drawing.Point(362, 218);
            this.tb47.Margin = new System.Windows.Forms.Padding(0);
            this.tb47.MaxLength = 1;
            this.tb47.Multiline = false;
            this.tb47.Name = "tb47";
            this.tb47.Size = new System.Drawing.Size(45, 45);
            this.tb47.TabIndex = 43;
            this.tb47.Text = "";
            // 
            // tb50
            // 
            this.tb50.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb50.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb50.Location = new System.Drawing.Point(29, 264);
            this.tb50.Margin = new System.Windows.Forms.Padding(0);
            this.tb50.MaxLength = 1;
            this.tb50.Multiline = false;
            this.tb50.Name = "tb50";
            this.tb50.Size = new System.Drawing.Size(45, 45);
            this.tb50.TabIndex = 45;
            this.tb50.Text = "";
            // 
            // tb51
            // 
            this.tb51.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb51.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb51.Location = new System.Drawing.Point(74, 264);
            this.tb51.Margin = new System.Windows.Forms.Padding(0);
            this.tb51.MaxLength = 1;
            this.tb51.Multiline = false;
            this.tb51.Name = "tb51";
            this.tb51.Size = new System.Drawing.Size(45, 45);
            this.tb51.TabIndex = 46;
            this.tb51.Text = "";
            // 
            // tb52
            // 
            this.tb52.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb52.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb52.Location = new System.Drawing.Point(119, 264);
            this.tb52.Margin = new System.Windows.Forms.Padding(0);
            this.tb52.MaxLength = 1;
            this.tb52.Multiline = false;
            this.tb52.Name = "tb52";
            this.tb52.Size = new System.Drawing.Size(45, 45);
            this.tb52.TabIndex = 47;
            this.tb52.Text = "";
            // 
            // tb53
            // 
            this.tb53.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb53.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb53.Location = new System.Drawing.Point(172, 264);
            this.tb53.Margin = new System.Windows.Forms.Padding(0);
            this.tb53.MaxLength = 1;
            this.tb53.Multiline = false;
            this.tb53.Name = "tb53";
            this.tb53.Size = new System.Drawing.Size(45, 45);
            this.tb53.TabIndex = 48;
            this.tb53.Text = "";
            // 
            // tb54
            // 
            this.tb54.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb54.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb54.Location = new System.Drawing.Point(217, 264);
            this.tb54.Margin = new System.Windows.Forms.Padding(0);
            this.tb54.MaxLength = 1;
            this.tb54.Multiline = false;
            this.tb54.Name = "tb54";
            this.tb54.Size = new System.Drawing.Size(45, 45);
            this.tb54.TabIndex = 49;
            this.tb54.Text = "";
            // 
            // tb55
            // 
            this.tb55.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb55.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb55.Location = new System.Drawing.Point(262, 264);
            this.tb55.Margin = new System.Windows.Forms.Padding(0);
            this.tb55.MaxLength = 1;
            this.tb55.Multiline = false;
            this.tb55.Name = "tb55";
            this.tb55.Size = new System.Drawing.Size(45, 45);
            this.tb55.TabIndex = 50;
            this.tb55.Text = "";
            // 
            // tb58
            // 
            this.tb58.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb58.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb58.Location = new System.Drawing.Point(407, 264);
            this.tb58.Margin = new System.Windows.Forms.Padding(0);
            this.tb58.MaxLength = 1;
            this.tb58.Multiline = false;
            this.tb58.Name = "tb58";
            this.tb58.Size = new System.Drawing.Size(45, 45);
            this.tb58.TabIndex = 53;
            this.tb58.Text = "";
            // 
            // tb56
            // 
            this.tb56.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb56.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb56.Location = new System.Drawing.Point(317, 264);
            this.tb56.Margin = new System.Windows.Forms.Padding(0);
            this.tb56.MaxLength = 1;
            this.tb56.Multiline = false;
            this.tb56.Name = "tb56";
            this.tb56.Size = new System.Drawing.Size(45, 45);
            this.tb56.TabIndex = 51;
            this.tb56.Text = "";
            // 
            // tb57
            // 
            this.tb57.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb57.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb57.Location = new System.Drawing.Point(362, 264);
            this.tb57.Margin = new System.Windows.Forms.Padding(0);
            this.tb57.MaxLength = 1;
            this.tb57.Multiline = false;
            this.tb57.Name = "tb57";
            this.tb57.Size = new System.Drawing.Size(45, 45);
            this.tb57.TabIndex = 52;
            this.tb57.Text = "";
            // 
            // tb60
            // 
            this.tb60.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb60.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb60.Location = new System.Drawing.Point(29, 318);
            this.tb60.Margin = new System.Windows.Forms.Padding(0);
            this.tb60.MaxLength = 1;
            this.tb60.Multiline = false;
            this.tb60.Name = "tb60";
            this.tb60.Size = new System.Drawing.Size(45, 45);
            this.tb60.TabIndex = 54;
            this.tb60.Text = "";
            // 
            // tb61
            // 
            this.tb61.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb61.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb61.Location = new System.Drawing.Point(74, 318);
            this.tb61.Margin = new System.Windows.Forms.Padding(0);
            this.tb61.MaxLength = 1;
            this.tb61.Multiline = false;
            this.tb61.Name = "tb61";
            this.tb61.Size = new System.Drawing.Size(45, 45);
            this.tb61.TabIndex = 55;
            this.tb61.Text = "";
            // 
            // tb62
            // 
            this.tb62.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb62.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb62.Location = new System.Drawing.Point(119, 318);
            this.tb62.Margin = new System.Windows.Forms.Padding(0);
            this.tb62.MaxLength = 1;
            this.tb62.Multiline = false;
            this.tb62.Name = "tb62";
            this.tb62.Size = new System.Drawing.Size(45, 45);
            this.tb62.TabIndex = 56;
            this.tb62.Text = "";
            // 
            // tb63
            // 
            this.tb63.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb63.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb63.Location = new System.Drawing.Point(172, 318);
            this.tb63.Margin = new System.Windows.Forms.Padding(0);
            this.tb63.MaxLength = 1;
            this.tb63.Multiline = false;
            this.tb63.Name = "tb63";
            this.tb63.Size = new System.Drawing.Size(45, 45);
            this.tb63.TabIndex = 57;
            this.tb63.Text = "";
            // 
            // tb64
            // 
            this.tb64.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb64.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb64.Location = new System.Drawing.Point(217, 318);
            this.tb64.Margin = new System.Windows.Forms.Padding(0);
            this.tb64.MaxLength = 1;
            this.tb64.Multiline = false;
            this.tb64.Name = "tb64";
            this.tb64.Size = new System.Drawing.Size(45, 45);
            this.tb64.TabIndex = 58;
            this.tb64.Text = "";
            // 
            // tb65
            // 
            this.tb65.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb65.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb65.Location = new System.Drawing.Point(262, 318);
            this.tb65.Margin = new System.Windows.Forms.Padding(0);
            this.tb65.MaxLength = 1;
            this.tb65.Multiline = false;
            this.tb65.Name = "tb65";
            this.tb65.Size = new System.Drawing.Size(45, 45);
            this.tb65.TabIndex = 59;
            this.tb65.Text = "";
            // 
            // tb68
            // 
            this.tb68.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb68.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb68.Location = new System.Drawing.Point(407, 318);
            this.tb68.Margin = new System.Windows.Forms.Padding(0);
            this.tb68.MaxLength = 1;
            this.tb68.Multiline = false;
            this.tb68.Name = "tb68";
            this.tb68.Size = new System.Drawing.Size(45, 45);
            this.tb68.TabIndex = 62;
            this.tb68.Text = "";
            // 
            // tb66
            // 
            this.tb66.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb66.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb66.Location = new System.Drawing.Point(317, 318);
            this.tb66.Margin = new System.Windows.Forms.Padding(0);
            this.tb66.MaxLength = 1;
            this.tb66.Multiline = false;
            this.tb66.Name = "tb66";
            this.tb66.Size = new System.Drawing.Size(45, 45);
            this.tb66.TabIndex = 60;
            this.tb66.Text = "";
            // 
            // tb67
            // 
            this.tb67.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb67.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb67.Location = new System.Drawing.Point(362, 318);
            this.tb67.Margin = new System.Windows.Forms.Padding(0);
            this.tb67.MaxLength = 1;
            this.tb67.Multiline = false;
            this.tb67.Name = "tb67";
            this.tb67.Size = new System.Drawing.Size(45, 45);
            this.tb67.TabIndex = 61;
            this.tb67.Text = "";
            // 
            // tb70
            // 
            this.tb70.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb70.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb70.Location = new System.Drawing.Point(29, 364);
            this.tb70.Margin = new System.Windows.Forms.Padding(0);
            this.tb70.MaxLength = 1;
            this.tb70.Multiline = false;
            this.tb70.Name = "tb70";
            this.tb70.Size = new System.Drawing.Size(45, 45);
            this.tb70.TabIndex = 63;
            this.tb70.Text = "";
            // 
            // tb71
            // 
            this.tb71.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb71.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb71.Location = new System.Drawing.Point(74, 364);
            this.tb71.Margin = new System.Windows.Forms.Padding(0);
            this.tb71.MaxLength = 1;
            this.tb71.Multiline = false;
            this.tb71.Name = "tb71";
            this.tb71.Size = new System.Drawing.Size(45, 45);
            this.tb71.TabIndex = 64;
            this.tb71.Text = "";
            // 
            // tb72
            // 
            this.tb72.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb72.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb72.Location = new System.Drawing.Point(119, 364);
            this.tb72.Margin = new System.Windows.Forms.Padding(0);
            this.tb72.MaxLength = 1;
            this.tb72.Multiline = false;
            this.tb72.Name = "tb72";
            this.tb72.Size = new System.Drawing.Size(45, 45);
            this.tb72.TabIndex = 65;
            this.tb72.Text = "";
            // 
            // tb73
            // 
            this.tb73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb73.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb73.Location = new System.Drawing.Point(172, 364);
            this.tb73.Margin = new System.Windows.Forms.Padding(0);
            this.tb73.MaxLength = 1;
            this.tb73.Multiline = false;
            this.tb73.Name = "tb73";
            this.tb73.Size = new System.Drawing.Size(45, 45);
            this.tb73.TabIndex = 66;
            this.tb73.Text = "";
            // 
            // tb74
            // 
            this.tb74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb74.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb74.Location = new System.Drawing.Point(217, 364);
            this.tb74.Margin = new System.Windows.Forms.Padding(0);
            this.tb74.MaxLength = 1;
            this.tb74.Multiline = false;
            this.tb74.Name = "tb74";
            this.tb74.Size = new System.Drawing.Size(45, 45);
            this.tb74.TabIndex = 67;
            this.tb74.Text = "";
            // 
            // tb75
            // 
            this.tb75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb75.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb75.Location = new System.Drawing.Point(262, 364);
            this.tb75.Margin = new System.Windows.Forms.Padding(0);
            this.tb75.MaxLength = 1;
            this.tb75.Multiline = false;
            this.tb75.Name = "tb75";
            this.tb75.Size = new System.Drawing.Size(45, 45);
            this.tb75.TabIndex = 68;
            this.tb75.Text = "";
            // 
            // tb78
            // 
            this.tb78.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb78.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb78.Location = new System.Drawing.Point(407, 364);
            this.tb78.Margin = new System.Windows.Forms.Padding(0);
            this.tb78.MaxLength = 1;
            this.tb78.Multiline = false;
            this.tb78.Name = "tb78";
            this.tb78.Size = new System.Drawing.Size(45, 45);
            this.tb78.TabIndex = 71;
            this.tb78.Text = "";
            // 
            // tb76
            // 
            this.tb76.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb76.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb76.Location = new System.Drawing.Point(317, 364);
            this.tb76.Margin = new System.Windows.Forms.Padding(0);
            this.tb76.MaxLength = 1;
            this.tb76.Multiline = false;
            this.tb76.Name = "tb76";
            this.tb76.Size = new System.Drawing.Size(45, 45);
            this.tb76.TabIndex = 69;
            this.tb76.Text = "";
            // 
            // tb77
            // 
            this.tb77.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb77.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb77.Location = new System.Drawing.Point(362, 364);
            this.tb77.Margin = new System.Windows.Forms.Padding(0);
            this.tb77.MaxLength = 1;
            this.tb77.Multiline = false;
            this.tb77.Name = "tb77";
            this.tb77.Size = new System.Drawing.Size(45, 45);
            this.tb77.TabIndex = 70;
            this.tb77.Text = "";
            // 
            // tb80
            // 
            this.tb80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb80.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb80.Location = new System.Drawing.Point(29, 410);
            this.tb80.Margin = new System.Windows.Forms.Padding(0);
            this.tb80.MaxLength = 1;
            this.tb80.Multiline = false;
            this.tb80.Name = "tb80";
            this.tb80.Size = new System.Drawing.Size(45, 45);
            this.tb80.TabIndex = 72;
            this.tb80.Text = "";
            // 
            // tb81
            // 
            this.tb81.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb81.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb81.Location = new System.Drawing.Point(74, 410);
            this.tb81.Margin = new System.Windows.Forms.Padding(0);
            this.tb81.MaxLength = 1;
            this.tb81.Multiline = false;
            this.tb81.Name = "tb81";
            this.tb81.Size = new System.Drawing.Size(45, 45);
            this.tb81.TabIndex = 73;
            this.tb81.Text = "";
            // 
            // tb82
            // 
            this.tb82.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb82.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb82.Location = new System.Drawing.Point(119, 410);
            this.tb82.Margin = new System.Windows.Forms.Padding(0);
            this.tb82.MaxLength = 1;
            this.tb82.Multiline = false;
            this.tb82.Name = "tb82";
            this.tb82.Size = new System.Drawing.Size(45, 45);
            this.tb82.TabIndex = 74;
            this.tb82.Text = "";
            // 
            // tb83
            // 
            this.tb83.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb83.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb83.Location = new System.Drawing.Point(172, 410);
            this.tb83.Margin = new System.Windows.Forms.Padding(0);
            this.tb83.MaxLength = 1;
            this.tb83.Multiline = false;
            this.tb83.Name = "tb83";
            this.tb83.Size = new System.Drawing.Size(45, 45);
            this.tb83.TabIndex = 75;
            this.tb83.Text = "";
            // 
            // tb84
            // 
            this.tb84.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb84.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb84.Location = new System.Drawing.Point(217, 410);
            this.tb84.Margin = new System.Windows.Forms.Padding(0);
            this.tb84.MaxLength = 1;
            this.tb84.Multiline = false;
            this.tb84.Name = "tb84";
            this.tb84.Size = new System.Drawing.Size(45, 45);
            this.tb84.TabIndex = 76;
            this.tb84.Text = "";
            // 
            // tb85
            // 
            this.tb85.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb85.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb85.Location = new System.Drawing.Point(262, 410);
            this.tb85.Margin = new System.Windows.Forms.Padding(0);
            this.tb85.MaxLength = 1;
            this.tb85.Multiline = false;
            this.tb85.Name = "tb85";
            this.tb85.Size = new System.Drawing.Size(45, 45);
            this.tb85.TabIndex = 77;
            this.tb85.Text = "";
            // 
            // tb88
            // 
            this.tb88.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb88.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb88.Location = new System.Drawing.Point(407, 410);
            this.tb88.Margin = new System.Windows.Forms.Padding(0);
            this.tb88.MaxLength = 1;
            this.tb88.Multiline = false;
            this.tb88.Name = "tb88";
            this.tb88.Size = new System.Drawing.Size(45, 45);
            this.tb88.TabIndex = 80;
            this.tb88.Text = "";
            // 
            // tb86
            // 
            this.tb86.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb86.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb86.Location = new System.Drawing.Point(317, 410);
            this.tb86.Margin = new System.Windows.Forms.Padding(0);
            this.tb86.MaxLength = 1;
            this.tb86.Multiline = false;
            this.tb86.Name = "tb86";
            this.tb86.Size = new System.Drawing.Size(45, 45);
            this.tb86.TabIndex = 78;
            this.tb86.Text = "";
            // 
            // tb87
            // 
            this.tb87.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.tb87.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb87.Location = new System.Drawing.Point(362, 410);
            this.tb87.Margin = new System.Windows.Forms.Padding(0);
            this.tb87.MaxLength = 1;
            this.tb87.Multiline = false;
            this.tb87.Name = "tb87";
            this.tb87.Size = new System.Drawing.Size(45, 45);
            this.tb87.TabIndex = 79;
            this.tb87.Text = "";
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(477, 134);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(185, 33);
            this.btnStart.TabIndex = 81;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(494, 183);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 17);
            this.label1.TabIndex = 82;
            this.label1.Text = "Bernard Kekelić, v1.0";
          
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(3, 3);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(216, 44);
            this.progressBar1.TabIndex = 83;
          
            // 
            // lb_notify
            // 
            this.lb_notify.AutoSize = true;
            this.lb_notify.Location = new System.Drawing.Point(249, 19);
            this.lb_notify.Name = "lb_notify";
            this.lb_notify.Size = new System.Drawing.Size(0, 17);
            this.lb_notify.TabIndex = 84;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.lb_notify);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Location = new System.Drawing.Point(-2, 468);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(711, 52);
            this.panel1.TabIndex = 85;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(687, 527);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tb87);
            this.Controls.Add(this.tb86);
            this.Controls.Add(this.tb88);
            this.Controls.Add(this.tb85);
            this.Controls.Add(this.tb84);
            this.Controls.Add(this.tb83);
            this.Controls.Add(this.tb82);
            this.Controls.Add(this.tb81);
            this.Controls.Add(this.tb80);
            this.Controls.Add(this.tb77);
            this.Controls.Add(this.tb76);
            this.Controls.Add(this.tb78);
            this.Controls.Add(this.tb75);
            this.Controls.Add(this.tb74);
            this.Controls.Add(this.tb73);
            this.Controls.Add(this.tb72);
            this.Controls.Add(this.tb71);
            this.Controls.Add(this.tb70);
            this.Controls.Add(this.tb67);
            this.Controls.Add(this.tb66);
            this.Controls.Add(this.tb68);
            this.Controls.Add(this.tb65);
            this.Controls.Add(this.tb64);
            this.Controls.Add(this.tb63);
            this.Controls.Add(this.tb62);
            this.Controls.Add(this.tb61);
            this.Controls.Add(this.tb60);
            this.Controls.Add(this.tb57);
            this.Controls.Add(this.tb56);
            this.Controls.Add(this.tb58);
            this.Controls.Add(this.tb55);
            this.Controls.Add(this.tb54);
            this.Controls.Add(this.tb53);
            this.Controls.Add(this.tb52);
            this.Controls.Add(this.tb51);
            this.Controls.Add(this.tb50);
            this.Controls.Add(this.tb47);
            this.Controls.Add(this.tb46);
            this.Controls.Add(this.tb48);
            this.Controls.Add(this.tb45);
            this.Controls.Add(this.tb44);
            this.Controls.Add(this.tb43);
            this.Controls.Add(this.tb42);
            this.Controls.Add(this.tb41);
            this.Controls.Add(this.tb40);
            this.Controls.Add(this.tb37);
            this.Controls.Add(this.tb36);
            this.Controls.Add(this.tb38);
            this.Controls.Add(this.tb35);
            this.Controls.Add(this.tb34);
            this.Controls.Add(this.tb33);
            this.Controls.Add(this.tb32);
            this.Controls.Add(this.tb31);
            this.Controls.Add(this.tb30);
            this.Controls.Add(this.tb27);
            this.Controls.Add(this.tb26);
            this.Controls.Add(this.tb28);
            this.Controls.Add(this.tb25);
            this.Controls.Add(this.tb24);
            this.Controls.Add(this.tb23);
            this.Controls.Add(this.tb22);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.tb20);
            this.Controls.Add(this.tb17);
            this.Controls.Add(this.tb16);
            this.Controls.Add(this.tb18);
            this.Controls.Add(this.tb15);
            this.Controls.Add(this.tb14);
            this.Controls.Add(this.tb13);
            this.Controls.Add(this.tb12);
            this.Controls.Add(this.tb11);
            this.Controls.Add(this.tb10);
            this.Controls.Add(this.tb08);
            this.Controls.Add(this.tb07);
            this.Controls.Add(this.tb06);
            this.Controls.Add(this.tb05);
            this.Controls.Add(this.tb04);
            this.Controls.Add(this.tb03);
            this.Controls.Add(this.tb02);
            this.Controls.Add(this.tb01);
            this.Controls.Add(this.tb00);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Sudoku Solver";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox tb00;
        private System.Windows.Forms.RichTextBox tb01;
        private System.Windows.Forms.RichTextBox tb02;
        private System.Windows.Forms.RichTextBox tb03;
        private System.Windows.Forms.RichTextBox tb04;
        private System.Windows.Forms.RichTextBox tb05;
        private System.Windows.Forms.RichTextBox tb06;
        private System.Windows.Forms.RichTextBox tb07;
        private System.Windows.Forms.RichTextBox tb08;
        private System.Windows.Forms.RichTextBox tb10;
        private System.Windows.Forms.RichTextBox tb11;
        private System.Windows.Forms.RichTextBox tb12;
        private System.Windows.Forms.RichTextBox tb13;
        private System.Windows.Forms.RichTextBox tb14;
        private System.Windows.Forms.RichTextBox tb15;
        private System.Windows.Forms.RichTextBox tb18;
        private System.Windows.Forms.RichTextBox tb16;
        private System.Windows.Forms.RichTextBox tb17;
        private System.Windows.Forms.RichTextBox tb20;
        private System.Windows.Forms.RichTextBox tb21;
        private System.Windows.Forms.RichTextBox tb22;
        private System.Windows.Forms.RichTextBox tb23;
        private System.Windows.Forms.RichTextBox tb24;
        private System.Windows.Forms.RichTextBox tb25;
        private System.Windows.Forms.RichTextBox tb28;
        private System.Windows.Forms.RichTextBox tb26;
        private System.Windows.Forms.RichTextBox tb27;
        private System.Windows.Forms.RichTextBox tb30;
        private System.Windows.Forms.RichTextBox tb31;
        private System.Windows.Forms.RichTextBox tb32;
        private System.Windows.Forms.RichTextBox tb33;
        private System.Windows.Forms.RichTextBox tb34;
        private System.Windows.Forms.RichTextBox tb35;
        private System.Windows.Forms.RichTextBox tb38;
        private System.Windows.Forms.RichTextBox tb36;
        private System.Windows.Forms.RichTextBox tb37;
        private System.Windows.Forms.RichTextBox tb40;
        private System.Windows.Forms.RichTextBox tb41;
        private System.Windows.Forms.RichTextBox tb42;
        private System.Windows.Forms.RichTextBox tb43;
        private System.Windows.Forms.RichTextBox tb44;
        private System.Windows.Forms.RichTextBox tb45;
        private System.Windows.Forms.RichTextBox tb48;
        private System.Windows.Forms.RichTextBox tb46;
        private System.Windows.Forms.RichTextBox tb47;
        private System.Windows.Forms.RichTextBox tb50;
        private System.Windows.Forms.RichTextBox tb51;
        private System.Windows.Forms.RichTextBox tb52;
        private System.Windows.Forms.RichTextBox tb53;
        private System.Windows.Forms.RichTextBox tb54;
        private System.Windows.Forms.RichTextBox tb55;
        private System.Windows.Forms.RichTextBox tb58;
        private System.Windows.Forms.RichTextBox tb56;
        private System.Windows.Forms.RichTextBox tb57;
        private System.Windows.Forms.RichTextBox tb60;
        private System.Windows.Forms.RichTextBox tb61;
        private System.Windows.Forms.RichTextBox tb62;
        private System.Windows.Forms.RichTextBox tb63;
        private System.Windows.Forms.RichTextBox tb64;
        private System.Windows.Forms.RichTextBox tb65;
        private System.Windows.Forms.RichTextBox tb68;
        private System.Windows.Forms.RichTextBox tb66;
        private System.Windows.Forms.RichTextBox tb67;
        private System.Windows.Forms.RichTextBox tb70;
        private System.Windows.Forms.RichTextBox tb71;
        private System.Windows.Forms.RichTextBox tb72;
        private System.Windows.Forms.RichTextBox tb73;
        private System.Windows.Forms.RichTextBox tb74;
        private System.Windows.Forms.RichTextBox tb75;
        private System.Windows.Forms.RichTextBox tb78;
        private System.Windows.Forms.RichTextBox tb76;
        private System.Windows.Forms.RichTextBox tb77;
        private System.Windows.Forms.RichTextBox tb80;
        private System.Windows.Forms.RichTextBox tb81;
        private System.Windows.Forms.RichTextBox tb82;
        private System.Windows.Forms.RichTextBox tb83;
        private System.Windows.Forms.RichTextBox tb84;
        private System.Windows.Forms.RichTextBox tb85;
        private System.Windows.Forms.RichTextBox tb88;
        private System.Windows.Forms.RichTextBox tb86;
        private System.Windows.Forms.RichTextBox tb87;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lb_notify;
        private System.Windows.Forms.Panel panel1;
    }
}

